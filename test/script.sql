USE [test]
GO
/****** Object:  Table [dbo].[authors]    Script Date: 9/12/2018 12:25:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[authors](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_authors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[books]    Script Date: 9/12/2018 12:25:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[books](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[categories_id] [int] NOT NULL,
	[authors_id] [int] NOT NULL,
 CONSTRAINT [PK_books] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[categories]    Script Date: 9/12/2018 12:25:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categories](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_categories] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[authors] ON 

INSERT [dbo].[authors] ([id], [name]) VALUES (1, N'Author 1')
INSERT [dbo].[authors] ([id], [name]) VALUES (2, N'Author 2')
INSERT [dbo].[authors] ([id], [name]) VALUES (3, N'Author 3')
SET IDENTITY_INSERT [dbo].[authors] OFF
SET IDENTITY_INSERT [dbo].[books] ON 

INSERT [dbo].[books] ([id], [name], [categories_id], [authors_id]) VALUES (1, N'Book 1', 1, 1)
INSERT [dbo].[books] ([id], [name], [categories_id], [authors_id]) VALUES (2, N'Book 2', 1, 1)
INSERT [dbo].[books] ([id], [name], [categories_id], [authors_id]) VALUES (3, N'Book 3', 2, 1)
INSERT [dbo].[books] ([id], [name], [categories_id], [authors_id]) VALUES (4, N'Book 4', 2, 2)
INSERT [dbo].[books] ([id], [name], [categories_id], [authors_id]) VALUES (5, N'Book 5', 3, 3)
INSERT [dbo].[books] ([id], [name], [categories_id], [authors_id]) VALUES (6, N'Book 6', 1, 1)
SET IDENTITY_INSERT [dbo].[books] OFF
SET IDENTITY_INSERT [dbo].[categories] ON 

INSERT [dbo].[categories] ([id], [name]) VALUES (1, N'Category 1')
INSERT [dbo].[categories] ([id], [name]) VALUES (2, N'Category 2')
INSERT [dbo].[categories] ([id], [name]) VALUES (3, N'Category 3')
SET IDENTITY_INSERT [dbo].[categories] OFF
ALTER TABLE [dbo].[books]  WITH CHECK ADD  CONSTRAINT [FK_books_authors] FOREIGN KEY([authors_id])
REFERENCES [dbo].[authors] ([id])
GO
ALTER TABLE [dbo].[books] CHECK CONSTRAINT [FK_books_authors]
GO
ALTER TABLE [dbo].[books]  WITH CHECK ADD  CONSTRAINT [FK_books_categories] FOREIGN KEY([categories_id])
REFERENCES [dbo].[categories] ([id])
GO
ALTER TABLE [dbo].[books] CHECK CONSTRAINT [FK_books_categories]
GO
