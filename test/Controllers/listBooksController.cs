﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace test.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using test.Models;

    [RoutePrefix("api/listBooks")]
    public class listBooksController : ApiController
    {
        // GET: api/listBooks
        [HttpGet]
        public async Task<List<Book>> Get()
        {
            testEntities entities = new testEntities();
            var books = await entities.books.Select(book =>
                new Book
                {
                    Author = new Author { Id = book.author.id, Name = book.author.name },
                    Category = new Category { Id = book.category.id, Name = book.category.name },
                    Id = book.id,
                    Name = book.name
                }).ToListAsync();
            return books;
        }

        public async Task<List<Book>> GetByCategory(int categoryId)
        {
            testEntities entities = new testEntities();
            var books = await entities.books.Where(book => book.category.id == categoryId).Select(book =>
                new Book
                {
                    Author = new Author { Id = book.author.id, Name = book.author.name },
                    Category = new Category { Id = book.category.id, Name = book.category.name },
                    Id = book.id,
                    Name = book.name
                }).ToListAsync();
            return books;
        }


        public async Task<List<Book>> GetByAuthor(int authorId)
        {
            testEntities entities = new testEntities();
            var books = await entities.books.Where(book => book.author.id == authorId).Select(book =>
                new Book
                {
                    Author = new Author { Id = book.author.id, Name = book.author.name },
                    Category = new Category { Id = book.category.id, Name = book.category.name },
                    Id = book.id,
                    Name = book.name
                }).ToListAsync();
            return books;
        }

    }
}
